package me.justicepro.chatserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/* Connection is a thread.
 * Threads allow you to do several things at the same time rather than one line at a time.
 * 
 * This class will handle one client.
 */
public class Connection extends Thread
{

	// Can be accessed from the entire class.
	private Socket socket;

	// Requires a socket.
	public Connection(Socket socket)
	{
		this.socket = socket;
	}

	// Executed when thread is started.
	@Override
	public void run()
	{
		// This will *try* executing the following and if there's an error it'll *catch* it and execute the code below instead of crashing.
		try
		{
			InputStream input = socket.getInputStream(); // Used to get information sent by the client.
			OutputStream output = socket.getOutputStream();
			
			DataInputStream dataInput = new DataInputStream(input);
			
			while (socket.isConnected()) // Keeps looping until the client disconnects or the server kicks the client.
			{
				try
				{
					String message = dataInput.readUTF();
					
					// Handle the data.
					for (Connection connection : ChatServer.connections) // Execute this for every connection.
					{
						DataOutputStream o = new DataOutputStream(connection.socket.getOutputStream()); // Get Output for the connection.
						o.writeUTF(message); // Turn the message into a byte array and queue it to be sent.
						o.flush(); // Send all queued messages.
					}
					
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		} catch (IOException e)
		{
			e.printStackTrace(); // Print error to console.
		}
	}

}