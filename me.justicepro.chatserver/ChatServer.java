package me.justicepro.chatserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ChatServer
{
	
	// This is a list of all connections.
	public static ArrayList<Connection> connections = new ArrayList<>();
	
	/* This executes when the program is started. */
	public static void main(String[] args) throws IOException
	{
		ServerSocket ss = new ServerSocket(8080); // Create the server on the port 8080.
		
		while (true) // Run Forever.
		{
			Socket socket = ss.accept(); // Accept client.
			Connection connection = new Connection(socket); // Create Connection Object.
			connections.add(connection); // Add connection to the list of connections.
			connection.start(); // Start the thread.
		}
		
	}
	
}