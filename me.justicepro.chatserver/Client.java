package me.justicepro.chatserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client
{

	public static void main(String[] args) throws UnknownHostException, IOException // Instead of a try and catch.
	{
		Scanner scanner = new Scanner(System.in); // This allows us to read the console.

		System.out.print("IP: "); // Sends a message without sending a new line.
		String ip = scanner.nextLine(); // Read one line.

		System.out.print("Name: "); // Sends a message without sending a new line.
		String nick = scanner.nextLine(); // Read one line.

		Socket socket = new Socket(ip, 8080); // Connect to the server.

		InputStream input = socket.getInputStream();
		OutputStream output = socket.getOutputStream();

		DataInputStream dataInput = new DataInputStream(input);
		DataOutputStream dataOutput = new DataOutputStream(output);

		Thread thread = new Thread(new Runnable() // This creates a thread.
				{
			public void run()
			{
				// This thread lets you read messages independently of sending messages.
				while (socket.isConnected())
				{
					try
					{
						String message = dataInput.readUTF(); // Read a message.

						System.out.println(message); // Display message.
					} catch (IOException e)
					{
						e.printStackTrace();
					}
				}

			}
				});

		thread.start(); // Start the thread.

		while (socket.isConnected())
		{
			String line = scanner.nextLine();
			
			dataOutput.writeUTF(nick + ": " + line);
			dataOutput.flush();
		}

	}

}